package utils

// GridSize represents the dimensions of a grid with a specified number of rows and columns.
//
// Fields:
// - Rows: The number of rows in the grid.
// - Cols: The number of columns in the grid.
//
// Example Usage:
//
// To create a new GridSize instance with 3 rows and 4 columns:
//
//	gridSize := GridSize{
//	    Rows: 3,
//	    Cols: 4,
//	}
//
// You can access the rows and columns using the respective fields:
//
//	numRows := gridSize.Rows
//	numCols := gridSize.Cols
type GridSize struct {
	Rows int // The number of rows in the grid.
	Cols int // The number of columns in the grid.
}
