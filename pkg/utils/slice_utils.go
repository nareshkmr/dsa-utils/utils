// Package utils provides utility functions for common operations.
package utils

// IndexOf returns the index of the first occurrence of the specified value in the given slice.
// If the value is not found, it returns -1.
//
// Parameters:
//   - slice: A slice of integers to be searched.
//   - value: The integer value to find within the slice.
//
// Returns:
//   - int: The index of the first occurrence of the value in the slice, or -1 if the value is not found.
//
// Example:
//
//	slice := []int{10, 20, 30, 40}
//	index := utils.IndexOf(slice, 30)
//	fmt.Println(index) // Output: 2
//
// Note:
//
//	This function performs a linear search, so the time complexity is O(n), where n is the length of the slice.
func IndexOf(slice []int, value int) int {
	for i, v := range slice {
		if v == value {
			return i
		}
	}
	return -1
}
