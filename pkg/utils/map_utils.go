package utils

// MapKeys returns a slice of all keys from the given map.
//
// This function takes a map as input and returns a slice containing all the keys from the map.
// It uses Go generics to support maps with any key and value types.
//
// Type Parameters:
//   - K: The type of the keys in the map. It must be a comparable type (e.g., int, string).
//   - V: The type of the values in the map. It can be any type.
//
// Parameters:
//   - m (map[K]V): The map from which keys will be extracted.
//
// Returns:
//   - []K: A slice containing all the keys from the input map.
//
// Example usage:
//
//	// Example map with string keys and int values
//	m := map[string]int{
//	  "apple":  5,
//	  "banana": 3,
//	  "cherry": 7,
//	}
//
//	// Get all keys
//	keys := MapKeys(m)
//
//	// Print the keys
//	fmt.Println("Keys:", keys) // Output: Keys: [apple banana cherry]
func MapKeys[K comparable, V any](m map[K]V) []K {
	keys := make([]K, 0, len(m))
	for key := range m {
		keys = append(keys, key)
	}
	return keys
}

// MapValues returns a slice of all values from the given map.
//
// This function takes a map as input and returns a slice containing all the values from the map.
// It uses Go generics to support maps with any key and value types.
//
// Type Parameters:
//   - K: The type of the keys in the map. It must be a comparable type (e.g., int, string).
//   - V: The type of the values in the map. It can be any type.
//
// Parameters:
//   - m (map[K]V): The map from which values will be extracted.
//
// Returns:
//   - []V: A slice containing all the values from the input map.
//
// Example usage:
//
//	// Example map with string keys and int values
//	m := map[string]int{
//	  "apple":  5,
//	  "banana": 3,
//	  "cherry": 7,
//	}
//
//	// Get all values
//	values := MapValues(m)
//
//	// Print the values
//	fmt.Println("Values:", values) // Output: Values: [5 3 7]
func MapValues[K comparable, V any](m map[K]V) []V {
	values := make([]V, 0, len(m))
	for _, value := range m {
		values = append(values, value)
	}
	return values
}
