package utils

// IntersectHashSet finds the intersection of two maps with boolean values as sets.
// It returns a slice of keys that are present in both maps with a value of true.
//
// Parameters:
// - map1: The first map with keys of type K and boolean values representing the set.
// - map2: The second map with keys of type K and boolean values representing the set.
//
// Returns:
// - A slice of keys of type K that are present in both map1 and map2 with a value of true.
func IntersectHashSet[K comparable](map1, map2 map[K]bool) []K {
	var intersection []K

	// Loop over the first map and check if the node is also `true` in the second map
	for key, value1 := range map1 {
		if value1 { // Check if the value is true in the first map
			if value2, found := map2[key]; found && value2 {
				intersection = append(intersection, key)
			}
		}
	}

	return intersection
}

// IntersectHashMap finds the intersection of two maps with any value type.
// It returns a slice of keys that are present in both maps with values that are considered equal
// based on the provided equals function.
//
// Parameters:
// - map1: The first map with keys of type K and values of type V.
// - map2: The second map with keys of type K and values of type V.
// - equals: A function that compares two values of type V and returns true if they are considered equal.
//
// Returns:
// - A slice of keys of type K that are present in both map1 and map2 with values that are equal according to the equals function.
func IntersectHashMap[K comparable, V any](map1, map2 map[K]V, equals func(V, V) bool) []K {
	var intersection []K

	// Loop over the first map and check if the node is also in the second map
	for key, value1 := range map1 {
		if value2, found := map2[key]; found && equals(value1, value2) {
			intersection = append(intersection, key)
		}
	}

	return intersection
}

// CopyMap copies all key-value pairs from the source map to the destination map.
//
// Parameters:
// - src: A pointer to the source map with keys and values of type int.
// - dst: A pointer to the destination map with keys and values of type int.
//
// The function does not return a value. It modifies the destination map in place,
// adding all key-value pairs from the source map.
func CopyMap(src, dst *map[int]int) {
	for key, value := range *src {
		(*dst)[key] = value
	}
}
