package utils

import "math"

// Coordinates represents a pair of coordinates in a grid or matrix.
// Fields:
// - R: The row index.
// - C: The column index.
//
// Example Usage:
// To create a new Coordinates instance with row 2 and column 3:
//
//	coord := Coordinates{
//	    R: 2,
//	    C: 3,
//	}
//
// You can access the row and column using the respective fields:
//
//	row := coord.R
//	col := coord.C
type Coordinates struct {
	R, C int // The row and column indices.
}

// MatrixNode represents a node in a matrix using an array of two integers.
// The first integer is the row index, and the second integer is the column index.
//
// Example Usage:
// To create a new MatrixNode instance for row 1, column 2:
//
//	node := MatrixNode{1, 2}
//
// You can access the row and column using array indexing:
//
//	row := node[0]
//	col := node[1]
type MatrixNode [2]int

// Directions defines the four possible movement directions in a grid or matrix.
// It is an array of Coordinates representing the following movements:
// - {1, 0}: Move down
// - {0, 1}: Move right
// - {-1, 0}: Move up
// - {0, -1}: Move left
//
// Example Usage:
// To use the Directions variable to move from a given coordinate:
//
//	current := Coordinates{R: 2, C: 2}
//	newRow := current.R + Directions[0].R // Move down
//	newCol := current.C + Directions[0].C // Move down
var Directions = [4]Coordinates{
	{1, 0},  // Move down
	{0, 1},  // Move right
	{-1, 0}, // Move up
	{0, -1}, // Move left
}

// IsOutOfBoundsOfMatrix checks whether a given position is out of the bounds of a matrix.
//
// Parameters:
// - pos: A Coordinates struct representing the position to check. It contains two fields:
//   - R: the row index
//   - C: the column index
//
// - gridSize: A pointer to a GridSize struct representing the size of the matrix. It contains two fields:
//   - Rows: the number of rows in the matrix
//   - Cols: the number of columns in the matrix
//
// Returns:
// - A boolean value: true if the position is out of the matrix bounds, false otherwise.
//
// Example:
//
//	pos := Coordinates{R: 3, C: 5}
//	gridSize := &GridSize{Rows: 4, Cols: 6}
//	outOfBounds := IsOutOfBoundsOfMatrix(pos, gridSize)
//	fmt.Println(outOfBounds) // Output: false
func IsOutOfBoundsOfMatrix(pos Coordinates, gridSize *GridSize) bool {
	if min(pos.R, pos.C) < 0 ||
		pos.R >= gridSize.Rows || pos.C >= gridSize.Cols {
		return true
	}
	return false
}

// CartesianCoordinates represents a pair of coordinates in the Cartesian plane.
// Fields:
// - X: The x-coordinate.
// - Y: The y-coordinate.
//
// Example Usage:
// To create a new CartesianCoordinates instance with x 2 and y 3:
//
//	coord := CartesianCoordinates{
//	    X: 2,
//	    Y: 3,
//	}
//
// You can access the x and y values using the respective fields:
//
//	x := coord.X
//	y := coord.Y
type CartesianCoordinates struct {
	X, Y int
}

// Distance calculates the Euclidean distance between two Cartesian coordinates.
//
// Parameters:
// - other: Another CartesianCoordinates instance to which the distance is calculated.
//
// Returns:
// - A float64 representing the Euclidean distance.
//
// Example Usage:
// point1 := CartesianCoordinates{X: 1, Y: 2}
// point2 := CartesianCoordinates{X: 4, Y: 6}
// distance := point1.Distance(point2)
// fmt.Println(distance) // Output: 5
func (c CartesianCoordinates) Distance(other CartesianCoordinates) float64 {
	return math.Sqrt(math.Pow(float64(c.X-other.X), 2) + math.Pow(float64(c.Y-other.Y), 2))
}
