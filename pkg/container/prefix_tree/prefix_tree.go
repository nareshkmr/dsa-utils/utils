// Package container provides utility functions and data structures for common tasks.
package container

// PrefixTree represents a prefix tree (trie) data structure.
// It is used to store and efficiently search for strings based on their prefixes.
//
// Fields:
// - root: A pointer to the root node of the prefix tree.
//
// Example usage:
//
//	trie := NewPrefixTree()
//	trie.Insert("hello")
//	exists := trie.Search("hello")
//	prefixExists := trie.StartsWith("he")
type PrefixTree struct {
	root *PrefixTreeNode
}

// NewPrefixTree creates and returns a new PrefixTree instance.
// The root node is initialized as a new PrefixTreeNode.
//
// Example usage:
//
//	trie := NewPrefixTree()
func NewPrefixTree() *PrefixTree {
	return &PrefixTree{
		root: NewPrefixTreeNode(),
	}
}

// Insert adds a word to the prefix tree.
// It iterates through each character of the word and creates new nodes as needed.
// Finally, it marks the last node as a complete word.
//
// Parameters:
// - word: The string to be inserted into the prefix tree.
//
// Example usage:
//
//	trie := NewPrefixTree()
//	trie.Insert("hello")
func (trie PrefixTree) Insert(word string) {
	curr := trie.root
	for _, char := range word {
		if _, exists := curr.Children[char]; !exists {
			curr.Children[char] = NewPrefixTreeNode()
		}
		curr = curr.Children[char]
	}
	curr.IsCompleteWord = true
}

// Search checks if a word exists in the prefix tree.
// It iterates through each character of the word, traversing the tree.
// It returns true if the word is found and is marked as a complete word; otherwise, it returns false.
//
// Parameters:
// - word: The string to search for in the prefix tree.
//
// Returns:
// - bool: True if the word exists and is complete; otherwise, false.
//
// Example usage:
//
//	trie := NewPrefixTree()
//	trie.Insert("hello")
//	exists := trie.Search("hello") // returns true
func (trie PrefixTree) Search(word string) bool {
	curr := trie.root
	for _, char := range word {
		if _, exists := curr.Children[char]; !exists {
			return false
		}
		curr = curr.Children[char]
	}
	return curr.IsCompleteWord
}

// StartsWith checks if there is any word in the prefix tree that starts with the given prefix.
// It iterates through each character of the prefix, traversing the tree.
// It returns true if the prefix is found; otherwise, it returns false.
//
// Parameters:
// - prefix: The string prefix to search for in the prefix tree.
//
// Returns:
// - bool: True if the prefix exists; otherwise, false.
//
// Example usage:
//
//	trie := NewPrefixTree()
//	trie.Insert("hello")
//	prefixExists := trie.StartsWith("he") // returns true
func (trie PrefixTree) StartsWith(prefix string) bool {
	curr := trie.root

	for _, char := range prefix {
		if _, exists := curr.Children[char]; !exists {
			return false
		}
		curr = curr.Children[char]
	}
	return true
}

// GetRoot returns the root node of the prefix tree.
//
// Returns:
// - *PrefixTreeNode: The root node of the prefix tree.
//
// Example usage:
//
//	trie := NewPrefixTree()
//	root := trie.GetRoot()
func (trie PrefixTree) GetRoot() *PrefixTreeNode {
	return trie.root
}
