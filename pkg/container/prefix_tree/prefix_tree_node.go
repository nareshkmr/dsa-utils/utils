// Package container provides utility functions and data structures for common tasks.
package container

// PrefixTreeNode represents a node in a prefix tree (trie) data structure.
// It is used to store strings in a way that allows efficient prefix-based searches.
//
// Fields:
// - children: A map where the keys are runes (characters) and the values are pointers to child PrefixTreeNodes.
// - isCompleteWord: A boolean flag indicating whether the path to this node represents a complete word.
//
// Example usage:
//
//	node := NewPrefixTreeNode()
//	node.children['a'] = NewPrefixTreeNode()
//	node.children['a'].isCompleteWord = true
type PrefixTreeNode struct {
	Children       map[rune]*PrefixTreeNode
	IsCompleteWord bool
}

// NewPrefixTreeNode creates and returns a new PrefixTreeNode instance.
// The children map is initialized as an empty map, and isCompleteWord is set to false.
//
// Example usage:
//
//	root := NewPrefixTreeNode()
func NewPrefixTreeNode() *PrefixTreeNode {
	return &PrefixTreeNode{
		Children:       make(map[rune]*PrefixTreeNode),
		IsCompleteWord: false,
	}
}
