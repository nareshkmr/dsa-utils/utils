// Package container provides various data structures and algorithms
// for general usage.
//
// The ListNode type represents a node in a singly linked list.
package container

import "fmt"

// ListNode represents a node in a singly linked list.
//
// The ListNode struct is a generic type, where T can be any type.
// Each ListNode contains a value of type T and a pointer to the next
// node in the list. The zero value for ListNode is an empty list ready
// to have elements added.
//
// Example usage:
//
//	// Creating a new list node
//	node := &ListNode[int]{Val: 10}
//	// Linking nodes
//	secondNode := &ListNode[int]{Val: 20}
//	node.Next = secondNode
//
// Fields:
// Val: The value stored in the node.
// Next: A pointer to the next node in the list, or nil if there is no next node.
type ListNode[T any] struct {
	Id   string
	Val  T            // The value stored in the node.
	Next *ListNode[T] // A pointer to the next node in the list.
}

// NewListNode creates and returns a new ListNode with the given value.
//
// The ID field of the ListNode is assigned a default value
// of the format "ListNode<Val>", where <Val> is the value
// of the node.
//
// Example usage:
//
//	node := NewListNode(10)
//	fmt.Println(node.ID)  // Output: ListNode10
//
// Parameters:
// val: The value to be stored in the new ListNode.
//
// Returns:
// A pointer to the newly created ListNode with the specified value and an assigned ID.
func NewListNode[T any](val T) *ListNode[T] {
	return &ListNode[T]{
		Val:  val,
		Id:   fmt.Sprintf("ListNode%v", val),
		Next: nil,
	}
}

// ArrayToLinkedList converts an array of values into a singly linked list.
//
// It takes a slice of values of any type T and returns a pointer to the
// head of the newly created linked list. The function iterates through
// the slice, creating a new ListNode for each value, and linking them together.
//
// Example usage:
//
//	values := []int{1, 2, 3, 4, 5}
//	head := ArrayToLinkedList(values)
//
// Parameters:
// arr: A slice of values to be converted into a linked list.
//
// Returns:
// A pointer to the head of the newly created linked list, or nil if the input slice is empty.
func ArrayToLinkedList[T any](arr []T) *ListNode[T] {
	if len(arr) == 0 {
		return nil
	}

	// Create the head of the linked list
	head := NewListNode(arr[0])
	current := head

	// Iterate through the array and create subsequent nodes
	for _, val := range arr[1:] {
		newNode := NewListNode(val)
		current.Next = newNode
		current = newNode
	}

	return head
}

// IsNil checks if the TreeNode itself is nil.
//
// The IsNil method works as follows:
//   - It checks if the pointer receiver is nil.
//   - If the pointer receiver is nil, IsNil returns true, indicating that
//     the TreeNode is nil. Otherwise, it returns false.
//
// Example usage:
//
// Checking if a TreeNode with an int value is nil:
//
//	var intNode *TreeNode[int]
//	if intNode.IsNil() {
//	    println("intNode is nil")
//	}
//
// Checking if a TreeNode with a string value is nil:
//
//	var stringNode *TreeNode[string]
//	if stringNode.IsNil() {
//	    println("stringNode is nil")
//	}
//
// The IsNil method is useful for determining if a TreeNode pointer is nil.
func (node *ListNode[T]) IsNil() bool {
	return node == nil
}
