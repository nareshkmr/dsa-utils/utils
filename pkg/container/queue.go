package container

import "errors"

// Queue represents a generic queue data structure that operates on a FIFO (First In, First Out) principle.
// It is implemented using a slice to store elements of any type.
//
// Fields:
// - items: A slice that holds the queue elements.
//
// Example Usage:
// To create a new Queue instance and perform enqueue, dequeue, peek, and other operations:
//
//	queue := NewQueue[int]()
//	queue.Enqueue(1)
//	queue.Enqueue(2)
//	front, _ := queue.Peek()  // front should be 1
//	size := queue.Size()      // size should be 2
//	dequeued, _ := queue.Dequeue() // dequeued should be 1
//	isEmpty := queue.IsEmpty() // isEmpty should be false
type Queue[T any] struct {
	items []T
}

// NewQueue creates a new queue and returns a pointer to it.
//
// Returns:
// - A pointer to a new, empty Queue instance.
func NewQueue[T any]() *Queue[T] {
	return &Queue[T]{}
}

// Enqueue adds an element to the back of the queue.
//
// Parameters:
// - item: The element of type T to be added to the queue.
func (q *Queue[T]) Enqueue(item T) {
	q.items = append(q.items, item)
}

// Dequeue removes and returns the front element of the queue.
//
// Returns:
// - The front element of type T if the queue is not empty.
// - An error if the queue is empty.
func (q *Queue[T]) Dequeue() (T, error) {
	if len(q.items) == 0 {
		var zero T
		return zero, errors.New("queue is empty")
	}
	item := q.items[0]
	q.items = q.items[1:]
	return item, nil
}

// Peek returns the front element of the queue without removing it.
//
// Returns:
// - The front element of type T if the queue is not empty.
// - An error if the queue is empty.
func (q *Queue[T]) Peek() (T, error) {
	if len(q.items) == 0 {
		var zero T
		return zero, errors.New("queue is empty")
	}
	return q.items[0], nil
}

// IsEmpty checks if the queue is empty.
//
// Returns:
// - True if the queue is empty.
// - False if the queue is not empty.
func (q *Queue[T]) IsEmpty() bool {
	return len(q.items) == 0
}

// Size returns the number of elements in the queue.
//
// Returns:
// - The number of elements in the queue.
func (q *Queue[T]) Size() int {
	return len(q.items)
}

// Items returns a copy of the elements in the queue.
//
// Returns:
// - A slice of elements in the queue.
func (q *Queue[T]) Items() []T {
	return append([]T(nil), q.items...)
}
