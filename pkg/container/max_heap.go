package container

// MaxHeap is a generic max-heap data structure that works with any ordered type.
// The ordering of elements is determined by the provided `greater` function.
//
// Fields:
// - data: A slice that holds the heap elements, with index 0 as a dummy.
// - greater: A function that takes two elements of type T and returns true if the first element is greater than the second.
type MaxHeap[T any] struct {
	data    []T
	greater func(a, b T) bool
}

// Len returns the number of elements in the heap.
//
// Returns:
// - The number of elements in the heap.
func (h MaxHeap[T]) Len() int {
	return len(h.data) - 1 // Adjust for the dummy element
}

// Greater compares the elements at indices i and j using the provided greater function.
//
// Parameters:
// - i: The index of the first element to compare.
// - j: The index of the second element to compare.
//
// Returns:
// - True if the element at index i is greater than the element at index j, otherwise false.
func (h MaxHeap[T]) Greater(i, j int) bool {
	return h.greater(h.data[i], h.data[j])
}

// Swap swaps the elements at indices i and j.
//
// Parameters:
// - i: The index of the first element to swap.
// - j: The index of the second element to swap.
func (h MaxHeap[T]) Swap(i, j int) {
	h.data[i], h.data[j] = h.data[j], h.data[i]
}

// Push adds an element to the heap.
//
// Parameters:
// - x: The element of type T to be added to the heap.
func (h *MaxHeap[T]) Push(x interface{}) {
	h.data = append(h.data, x.(T))
	h.percolateUp(len(h.data) - 1)
}

// Pop removes and returns the maximum element from the heap.
//
// Returns:
// - The maximum element of type T if the heap is not empty.
func (h *MaxHeap[T]) Pop() interface{} {
	if len(h.data) <= 1 {
		panic("heap is empty")
	}
	n := len(h.data)
	h.Swap(1, n-1)
	max := h.data[n-1]
	h.data = h.data[:n-1] // Reduce the heap size
	h.percolateDown(1, n-1)
	return max
}

// Top returns the maximum element from the heap without removing it.
//
// Returns:
// - The maximum element of type T if the heap is not empty.
// - Panics if the heap is empty.
func (h *MaxHeap[T]) Top() T {
	if len(h.data) <= 1 {
		panic("heap is empty")
	}
	return h.data[1]
}

// NewMaxHeap creates a new max-heap and returns a pointer to it.
//
// Parameters:
// - greater: A function that takes two elements of type T and returns true if the first element is greater than the second.
//
// Returns:
// - A pointer to a new, empty MaxHeap instance.
func NewMaxHeap[T any](greater func(a, b T) bool) *MaxHeap[T] {
	var zero T
	return &MaxHeap[T]{
		data:    []T{zero}, // Initialize with a zero value of T at index 0
		greater: greater,
	}
}

// Items returns a copy of the elements in the heap.
//
// Returns:
// - A slice of elements in the heap.
func (h *MaxHeap[T]) Items() []T {
	return append([]T(nil), h.data[1:]...)
}

// Heapify transforms an unsorted slice into a valid heap.
//
// Parameters:
// - data: A slice of elements of type T to be heapified.
func (h *MaxHeap[T]) Heapify(data []T) {
	var zero T
	h.data = append([]T{zero}, data...) // Prepend the zero value
	length := len(h.data)
	for idx := length / 2; idx >= 1; idx-- {
		h.percolateDown(idx, length)
	}
}

// percolateDown maintains the heap property by moving the element at index idx down to its correct position.
//
// Parameters:
// - idx: The index of the element to be percolated down.
// - length: The number of elements in the heap.
func (h *MaxHeap[T]) percolateDown(idx, length int) {
	for {
		leftChildIdx := 2 * idx
		rightChildIdx := leftChildIdx + 1
		parentIdx := idx
		if leftChildIdx < length && h.greater(h.data[leftChildIdx], h.data[parentIdx]) {
			parentIdx = leftChildIdx
		}
		if rightChildIdx < length && h.greater(h.data[rightChildIdx], h.data[parentIdx]) {
			parentIdx = rightChildIdx
		}
		if parentIdx == idx {
			break
		}
		h.Swap(idx, parentIdx)
		idx = parentIdx
	}
}

// percolateUp maintains the heap property by moving the element at index idx up to its correct position.
//
// Parameters:
// - idx: The index of the element to be percolated up.
func (h *MaxHeap[T]) percolateUp(idx int) {
	for {
		parent := idx / 2
		if idx == 1 || h.greater(h.data[parent], h.data[idx]) {
			break
		}
		h.Swap(idx, parent)
		idx = parent
	}
}

// ToSortedSlice converts the max-heap into a sorted slice and returns it.
//
// Returns:
// - A slice of elements of type T representing the heap's elements in sorted order.
func (h *MaxHeap[T]) ToSortedSlice() []T {
	// Create a copy of the heap to avoid modifying the original heap.
	tempHeap := MaxHeap[T]{
		data:    make([]T, len(h.data)),
		greater: h.greater,
	}
	copy(tempHeap.data, h.data)

	var sortedSlice []T
	for tempHeap.Len() > 0 {
		sortedSlice = append(sortedSlice, tempHeap.Pop().(T))
	}
	return sortedSlice
}
