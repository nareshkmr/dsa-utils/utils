package container

import "errors"

// Stack represents a generic stack data structure that operates on a LIFO (Last In, First Out) principle.
// It is implemented using a slice to store elements of any type.
//
// Fields:
// - items: A slice that holds the stack elements.
//
// Example Usage:
// To create a new Stack instance and perform push, pop, peek, top, at, and other operations:
//
//	stack := NewStack[int]()
//	stack.Push(1)
//	stack.Push(2)
//	top, _ := stack.Peek()  // top should be 2
//	peeked, _ := stack.Top() // peeked should be 2
//	penultimate, _ := stack.At(-2) // penultimate should be 1
//	first, _ := stack.At(0) // first should be 1
//	second, _ := stack.At(1) // second should be 2
//	size := stack.Size()    // size should be 2
//	popped, _ := stack.Pop() // popped should be 2
//	isEmpty := stack.IsEmpty() // isEmpty should be false
type Stack[T any] struct {
	items []T
}

// NewStack creates a new stack and returns a pointer to it.
//
// Returns:
// - A pointer to a new, empty Stack instance.
func NewStack[T any]() *Stack[T] {
	return &Stack[T]{}
}

// Push adds an element to the top of the stack.
//
// Parameters:
// - item: The element of type T to be added to the stack.
func (s *Stack[T]) Push(item T) {
	s.items = append(s.items, item)
}

// Pop removes and returns the top element of the stack.
//
// Returns:
// - The top element of type T if the stack is not empty.
// - An error if the stack is empty.
func (s *Stack[T]) Pop() (T, error) {
	if len(s.items) == 0 {
		var zero T
		return zero, errors.New("stack is empty")
	}
	n := len(s.items) - 1
	item := s.items[n]
	s.items = s.items[:n]
	return item, nil
}

// Peek returns the top element of the stack without removing it.
//
// Returns:
// - The top element of type T if the stack is not empty.
// - An error if the stack is empty.
func (s *Stack[T]) Peek() (T, error) {
	if len(s.items) == 0 {
		var zero T
		return zero, errors.New("stack is empty")
	}
	return s.items[len(s.items)-1], nil
}

// Top returns the top element of the stack without removing it.
//
// Returns:
// - The top element of type T if the stack is not empty.
// - An error if the stack is empty.
func (s *Stack[T]) Top() (T, error) {
	return s.Peek()
}

// At returns the element at the given index of the stack.
// If the index is negative, it indexes from the end of the stack.
//
// Parameters:
// - index: The index of the element to return. A negative index counts from the end of the stack.
//
// Returns:
// - The element of type T at the given index if the index is within bounds.
// - An error if the index is out of bounds.
func (s *Stack[T]) At(index int) (T, error) {
	n := len(s.items)
	if index < 0 {
		index = n + index
	}
	if index < 0 || index >= n {
		var zero T
		return zero, errors.New("index out of bounds")
	}
	return s.items[index], nil
}

// IsEmpty checks if the stack is empty.
//
// Returns:
// - True if the stack is empty.
// - False if the stack is not empty.
func (s *Stack[T]) IsEmpty() bool {
	return len(s.items) == 0
}

// Size returns the number of elements in the stack.
//
// Returns:
// - The number of elements in the stack.
func (s *Stack[T]) Size() int {
	return len(s.items)
}

// Items returns a copy of the elements in the stack.
//
// Returns:
// - A slice of elements in the stack.
func (s *Stack[T]) Items() []T {
	return append([]T(nil), s.items...)
}
