package container

import "reflect"

// TreeNode is a generic struct that represents a node in a binary tree.
// It can store values of any type specified by the type parameter T.
//
// The TreeNode struct is defined as follows:
//
//   - Val: This field stores the value of the node. The type of this value is
//     specified by the type parameter T. It allows the TreeNode to hold any
//     data type, making it highly versatile and reusable.
//
//   - Left: This field is a pointer to a TreeNode element. It represents the
//     child node to the left of the current node. In a standard binary tree,
//     there would be at most one left child.
//
//   - Right: This field is a pointer to a TreeNode element. It represents the
//     child node to the right of the current node. In a standard binary tree,
//     there would be at most one right child.
//
// Example usage:
//
// Creating a TreeNode to store integer values:
//
//	intNode := TreeNode[int]{Val: 5}
//	intNode.Left = &TreeNode[int]{Val: 3}
//	intNode.Right = &TreeNode[int]{Val: 8}
//
// Creating a TreeNode to store string values:
//
//	stringNode := TreeNode[string]{Val: "root"}
//	stringNode.Left = &TreeNode[string]{Val: "left"}
//	stringNode.Right = &TreeNode[string]{Val: "right"}
//
// The above examples demonstrate how the TreeNode struct can be instantiated
// with different types for the Val field, showcasing its flexibility and
// usefulness for various data structures and algorithms.
type TreeNode[T any] struct {
	Val   T
	Left  *TreeNode[T]
	Right *TreeNode[T]
}

// IsEmpty checks if the value of the TreeNode is the zero value for its type.
//
// The IsEmpty method works as follows:
//   - It creates a variable zero of the generic type T and initializes it to
//     the zero value of that type.
//   - It then uses the reflect.DeepEqual function to compare the Val field
//     of the TreeNode with the zero value.
//   - If Val is equal to the zero value, IsEmpty returns true, indicating that
//     the TreeNode's value is uninitialized or empty. Otherwise, it returns false.
//
// Example usage:
//
// Checking if a TreeNode with an int value is empty:
//
//	intNode := TreeNode[int]{}
//	if intNode.IsEmpty() {
//	    println("intNode.Val is zero value")
//	}
//
// Checking if a TreeNode with a string value is empty:
//
//	stringNode := TreeNode[string]{}
//	if stringNode.IsEmpty() {
//	    println("stringNode.Val is zero value")
//	}
//
// Checking if a TreeNode with a custom struct value is empty:
//
//	type MyStruct struct {
//	    Field1 int
//	    Field2 string
//	}
//	structNode := TreeNode[MyStruct]{}
//	if structNode.IsEmpty() {
//	    println("structNode.Val is zero value")
//	}
//
// The IsEmpty method is useful for determining if a TreeNode has been
// initialized with a meaningful value or if it still holds the default zero
// value for its type.
func (node TreeNode[T]) IsEmpty() bool {
	var zero T
	return reflect.DeepEqual(node.Val, zero)
}

// IsNil checks if the TreeNode itself is nil.
//
// The IsNil method works as follows:
//   - It checks if the pointer receiver is nil.
//   - If the pointer receiver is nil, IsNil returns true, indicating that
//     the TreeNode is nil. Otherwise, it returns false.
//
// Example usage:
//
// Checking if a TreeNode with an int value is nil:
//
//	var intNode *TreeNode[int]
//	if intNode.IsNil() {
//	    println("intNode is nil")
//	}
//
// Checking if a TreeNode with a string value is nil:
//
//	var stringNode *TreeNode[string]
//	if stringNode.IsNil() {
//	    println("stringNode is nil")
//	}
//
// The IsNil method is useful for determining if a TreeNode pointer is nil.
func (node *TreeNode[T]) IsNil() bool {
	return node == nil
}

// NewTreeNode is a constructor function that creates and returns a pointer to a new TreeNode
// initialized with the given value.
//
// Example usage:
//
// Creating a TreeNode to store integer values:
//
//	intNode := NewTreeNode(5)
//
// Creating a TreeNode to store string values:
//
//	stringNode := NewTreeNode("root")
func NewTreeNode[T any](value T) *TreeNode[T] {
	return &TreeNode[T]{
		Val:   value,
		Left:  nil,
		Right: nil,
	}
}
