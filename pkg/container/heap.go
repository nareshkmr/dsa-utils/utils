package container

// Heap is a generic min-heap data structure that works with any ordered type.
// The ordering of elements is determined by the provided `less` function.
//
// Fields:
// - data: A slice that holds the heap elements, with index 0 as a dummy.
// - less: A function that takes two elements of type T and returns true if the first element is less than the second.
type Heap[T any] struct {
	data []T
	less func(a, b T) bool
}

// Len returns the number of elements in the heap.
//
// Returns:
// - The number of elements in the heap.
func (h Heap[T]) Len() int {
	return len(h.data) - 1 // Adjust for the dummy element
}

// Less compares the elements at indices i and j using the provided less function.
//
// Parameters:
// - i: The index of the first element to compare.
// - j: The index of the second element to compare.
//
// Returns:
// - True if the element at index i is less than the element at index j, otherwise false.
func (h Heap[T]) Less(i, j int) bool {
	return h.less(h.data[i], h.data[j])
}

// Swap swaps the elements at indices i and j.
//
// Parameters:
// - i: The index of the first element to swap.
// - j: The index of the second element to swap.
func (h Heap[T]) Swap(i, j int) {
	h.data[i], h.data[j] = h.data[j], h.data[i]
}

// Push adds an element to the heap.
//
// Parameters:
// - x: The element of type T to be added to the heap.
func (h *Heap[T]) Push(x interface{}) {
	h.data = append(h.data, x.(T))
	h.percolateUp(len(h.data) - 1)
}

// Pop removes and returns the minimum element from the heap.
//
// Returns:
// - The minimum element of type T if the heap is not empty.
func (h *Heap[T]) Pop() interface{} {
	if len(h.data) <= 1 {
		panic("heap is empty")
	}
	n := len(h.data)
	h.Swap(1, n-1)
	min := h.data[n-1]
	h.data = h.data[:n-1] // Reduce the heap size
	h.percolateDown(1, n-1)
	return min
}

// Top returns the minimum element from the heap without removing it.
//
// Returns:
// - The minimum element of type T if the heap is not empty.
// - Panics if the heap is empty.
func (h *Heap[T]) Top() T {
	if len(h.data) <= 1 {
		panic("heap is empty")
	}
	return h.data[1]
}

// NewHeap creates a new heap and returns a pointer to it.
//
// Parameters:
// - less: A function that takes two elements of type T and returns true if the first element is less than the second.
//
// Returns:
// - A pointer to a new, empty Heap instance.
func NewHeap[T any](less func(a, b T) bool) *Heap[T] {
	var zero T
	return &Heap[T]{
		data: []T{zero}, // Initialize with a zero value of T at index 0
		less: less,
	}
}

// Items returns a copy of the elements in the heap.
//
// Returns:
// - A slice of elements in the heap.
func (h *Heap[T]) Items() []T {
	return append([]T(nil), h.data[1:]...)
}

// Heapify transforms an unsorted slice into a valid heap.
//
// Parameters:
// - data: A slice of elements of type T to be heapified.
func (h *Heap[T]) Heapify(data []T) {
	var zero T
	h.data = append([]T{zero}, data...) // Prepend the zero value
	length := len(h.data)
	for idx := length / 2; idx >= 1; idx-- {
		h.percolateDown(idx, length)
	}
}

// percolateDown maintains the heap property by moving the element at index idx down to its correct position.
//
// Parameters:
// - idx: The index of the element to be percolated down.
// - length: The number of elements in the heap.
func (h *Heap[T]) percolateDown(idx, length int) {
	for {
		leftChildIdx := 2 * idx
		rightChildIdx := 2*idx + 1
		smallest := idx
		if leftChildIdx < length && h.less(h.data[leftChildIdx], h.data[smallest]) {
			smallest = leftChildIdx
		}
		if rightChildIdx < length && h.less(h.data[rightChildIdx], h.data[smallest]) {
			smallest = rightChildIdx
		}
		if smallest == idx {
			break
		}
		h.Swap(idx, smallest)
		idx = smallest
	}
}

// percolateUp maintains the heap property by moving the element at index idx up to its correct position.
//
// Parameters:
// - idx: The index of the element to be percolated up.
func (h *Heap[T]) percolateUp(idx int) {
	for {
		parent := idx / 2
		if idx == 1 || h.less(h.data[parent], h.data[idx]) {
			break
		}
		h.Swap(idx, parent)
		idx = parent
	}
}

// ToSortedSlice converts the heap into a sorted slice and returns it.
//
// Returns:
// - A slice of elements of type T representing the heap's elements in sorted order.
func (h *Heap[T]) ToSortedSlice() []T {
	// Create a copy of the heap to avoid modifying the original heap.
	tempHeap := Heap[T]{
		data: make([]T, len(h.data)),
		less: h.less,
	}
	copy(tempHeap.data, h.data)

	var sortedSlice []T
	for tempHeap.Len() > 0 {
		sortedSlice = append(sortedSlice, tempHeap.Pop().(T))
	}
	return sortedSlice
}
