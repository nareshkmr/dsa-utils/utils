package container

// VisitedMap is a generic map type used to keep track of visited elements.
// It maps keys of any comparable type to a boolean value indicating whether the key has been visited.
// Typically, true means the key has been visited, while false or the absence of a key means it has not.
//
// Example Usage:
// To create a new VisitedMap and mark elements as visited:
//
//	visited := VisitedMap[int]{}
//	visited[1] = true  // Mark element 1 as visited
//	visited[2] = true  // Mark element 2 as visited
//	if visited[3] {    // Check if element 3 has been visited
//	    // Element 3 has been visited
//	} else {
//	    // Element 3 has not been visited
//	}
type VisitedMap[K comparable] map[K]bool

// HashSet is a generic set type implemented using a map.
// It maps keys of any comparable type to a boolean value indicating the presence of the key in the set.
// Typically, true means the key is present in the set, while false or the absence of a key means it is not.
//
// Example Usage:
// To create a new HashSet and add elements to it:
//
//	set := HashSet[int]{}
//	set[1] = true  // Add element 1 to the set
//	set[2] = true  // Add element 2 to the set
//	if set[3] {    // Check if element 3 is in the set
//	    // Element 3 is in the set
//	} else {
//	    // Element 3 is not in the set
//	}
type HashSet[K comparable] map[K]bool
