package container

// UnionFind implements the Union-Find data structure (also known as Disjoint Set Union, DSU)
// with path compression and union by rank optimization. It is used to efficiently manage
// a partition of a set into disjoint subsets.
//
// Fields:
//   - parent: A slice that holds the parent of each element. For an element x, parent[x] is
//     the parent of x. If parent[x] == x, then x is a root element.
//   - rank: A slice that holds the rank (approximate depth) of each tree. The rank is used to
//     keep the tree shallow by attaching smaller trees under the root of larger trees.
//
// Example Usage:
// To create a new UnionFind instance with 10 elements:
//
//	uf := NewUnionFind(10)
//
// To perform union and find operations:
//
//	uf.Union(1, 2)
//	uf.Union(3, 4)
//	root := uf.Find(1)
type UnionFind struct {
	parent, rank []int
}

// NewUnionFind creates a new UnionFind instance with a given size.
// Each element is initially its own parent, representing disjoint sets.
//
// Parameters:
// - size: The number of elements in the UnionFind structure.
//
// Returns:
// - A pointer to a new UnionFind instance with `size` elements.
func NewUnionFind(size int) *UnionFind {
	_parent := make([]int, size)
	_rank := make([]int, size)

	// Initially, there are no unions of nodes; so the parent of each node is themselves
	for idx := range _parent {
		_parent[idx] = idx
	}

	return &UnionFind{
		parent: _parent,
		rank:   _rank,
	}
}

// Find uses path compression to find the root parent of the given node.
// Path compression flattens the structure of the tree, making future
// operations faster.
//
// Parameters:
// - x: The element whose root parent is to be found.
//
// Returns:
// - The root parent of the element x.
func (uf *UnionFind) Find(x int) int {
	if uf.parent[x] != x {
		// Path compression to reach the highest parent node
		uf.parent[x] = uf.Find(uf.parent[x])
	}
	return uf.parent[x]
}

// Union merges the sets containing x and y. It uses the union by rank strategy
// to keep the tree heights low. In union by rank, the root of the tree with
// lower rank points to the root of the tree with higher rank. If the ranks
// are the same, one root points to the other, and the rank of the resulting root
// is incremented by one. This function ensures the UnionFind structure remains
// balanced, improving the efficiency of future Find and Union operations.
//
// Parameters:
// - x: An element from the first set.
// - y: An element from the second set.
//
// Returns:
//   - bool: Returns true if the union was performed (i.e., x and y were in different sets).
//     Returns false if no union was needed because x and y were already in the same set.
//
// The function performs the following steps:
//  1. Finds the roots (representatives) of the sets containing x and y using the path
//     compressed Find method.
//  2. If the roots are different, it performs a union:
//     - If the rank of rootX is greater than rootY, rootY's parent is set to rootX.
//     - If the rank of rootY is greater than rootX, rootX's parent is set to rootY.
//     - If the ranks are the same, rootY's parent is set to rootX and rootX's rank
//     is incremented by one.
//  3. Returns true if a union was made, otherwise false if x and y were already in the same set.
func (uf *UnionFind) Union(x, y int) bool {
	rootX := uf.Find(x)
	rootY := uf.Find(y)

	if rootX != rootY {
		if uf.rank[rootX] > uf.rank[rootY] {
			uf.parent[rootY] = rootX
		} else if uf.rank[rootX] < uf.rank[rootY] {
			uf.parent[rootX] = rootY
		} else {
			uf.parent[rootY] = rootX
			uf.rank[rootX]++
		}
		return true
	}
	return false
}

// GetUniqueParents returns a slice of unique parent elements from the union-find data structure.
//
// This function iterates through the parent array of the union-find structure,
// finds the root representative for each element, collects unique root elements,
// and returns them as a slice. Each unique parent represents a distinct set
// in the union-find structure.
//
// Returns:
//   - []int: A slice containing the unique parent elements.
//
// Example usage:
//
//	uf := NewUnionFind(10) // Assume a union-find structure with 10 elements
//	uf.Union(1, 2)
//	uf.Union(3, 4)
//	uf.Union(1, 4)
//
//	uniqueParents := uf.GetUniqueParents()
//	fmt.Println("Unique Parents:", uniqueParents) // Output: Unique Parents: [root1, root2, ...]
//
// Note: The exact output may vary depending on the union operations performed.
func (uf *UnionFind) GetUniqueParents() []int {
	uniqueParents := make(map[int]bool)
	for i := range uf.parent {
		root := uf.Find(i)
		uniqueParents[root] = true
	}
	parents := []int{}
	for parent := range uniqueParents {
		parents = append(parents, parent)
	}

	return parents
}
