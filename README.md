# Go Utils

[![Go Reference](https://pkg.go.dev/badge/github.com/yourusername/go-utils.svg)](https://pkg.go.dev/github.com/yourusername/go-utils)
[![Go Report Card](https://goreportcard.com/badge/github.com/yourusername/go-utils)](https://goreportcard.com/report/github.com/yourusername/go-utils)

A collection of utility data structures and algorithms implemented in Go. This package includes generic implementations of stacks, queues, heaps, hash sets, union-find, prefix trees, and other useful data structures.

## Features

- **Stack**: A generic stack (LIFO) implementation.
- **Queue**: A generic queue (FIFO) implementation.
- **MinHeap**: A generic min-heap implementation.
- **MaxHeap**: A generic max-heap implementation.
- **HashSet**: A generic hash set implementation.
- **UnionFind**: A union-find (disjoint set) data structure.
- **PrefixTree**: A prefix tree (trie) for efficient string storage and prefix-based searches.
- **Utility Functions**:
  - **IntersectMap**: Finds the intersection of two maps with keys of any comparable type.
  - **IntersectHashSet**: Finds the intersection of two maps with boolean values as sets.
  - **CopyMap**: Copies all key-value pairs from the source map to the destination map.
  - **Coordinates**: Represents a pair of coordinates.
  - **GridSize**: Represents the dimensions of a grid.
  - **MatrixNode**: Represents a node in a matrix using an array of two integers.
  - **Directions**: Defines the four possible movement directions in a grid or matrix.
  - **IsOutOfBoundsOfMatrix**: Checks if a given position is out of the bounds of a matrix.
  - **MapKeys**: Returns a slice of all keys from a given map.
  - **MapValues**: Returns a slice of all values from a given map.

## Installation

To install the package, run:

```sh
go get github.com/yourusername/go-utils@v1.3.0
```

<h2 style="text-align:center;">Naresh Kumar</h2>
<p style="text-align:center;">Senior Software Developer</p>
